# ax594
[![Go Reference](https://pkg.go.dev/badge/gitlab.snt.utwente.nl/silke/ax594.svg)](https://pkg.go.dev/gitlab.snt.utwente.nl/silke/ax594)
[![pipeline status](https://gitlab.snt.utwente.nl/silke/ax594/badges/master/pipeline.svg)](https://gitlab.snt.utwente.nl/silke/ax594/commits/master)
[![coverage report](https://gitlab.snt.utwente.nl/silke/ax594/badges/master/coverage.svg)](https://gitlab.snt.utwente.nl/silke/ax594/commits/master)

Tools for reading measurement values from the AXIOMET AX-594.

## Install

Download and install the library and tools using:

```
go get gitlab.snt.utwente.nl/silke/ax594/...
```

To use the library, use:

```go
import "gitlab.snt.utwente.nl/silke/ax594"
```

## Download

The latest builds can be downloaded from the following links:

- [Linux AMD64](https://gitlab.snt.utwente.nl/silke/ax594/-/jobs/artifacts/master/browse?job=build:linux:amd64)
- [Linux ARM](https://gitlab.snt.utwente.nl/silke/ax594/-/jobs/artifacts/master/browse?job=build:linux:arm)
- [Linux ARM64](https://gitlab.snt.utwente.nl/silke/ax594/-/jobs/artifacts/master/browse?job=build:linux:arm64)
- [Windows AMD64](https://gitlab.snt.utwente.nl/silke/ax594/-/jobs/artifacts/master/browse?job=build:windows:amd64)
