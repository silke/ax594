/*
Package ax594 implements the decoding of the measurement values for the AXIOMET AX-594,
when used with a serial connection.

The wire format is decoded as follows:

	   7    6    5    4    3    2    1    0
	 0 +----+----+----+----+----+----+----+----+
	   |             Sign (ASCII)              |
	 1 +----+----+----+----+----+----+----+----+
	   |         Decimal 1..4 (ASCII)          |
	   |                 ....                  |
	 5 +----+----+----+----+----+----+----+----+
	   |             Space (0x20)              |
	 6 +----+----+----+----+----+----+----+----+
	   |  0 |  0 |  1 |  1 |  0 | p3 | p2 | p1 | Point location
	 7 +----+----+----+----+----+----+----+----+
	   |    |    |auto| DC | AC | rel|hold|bar0|
	 8 +----+----+----+----+----+----+----+----+
	   |    |    | max| min|    |    | n  |    |
	 9 +----+----+----+----+----+----+----+----+
	   | µ  | m  | k  | M  |Beep|Dio | %  |    |
	10 +----+----+----+----+----+----+----+----+
	   | V  | A  |Ohm |    | Hz | F  | °C | °F |
	11 +----+----+----+----+----+----+----+----+
	   |           Bottom bar (int8)           |
	12 +----+----+----+----+----+----+----+----+
	   |               CR (0x0D)               |
	13 +----+----+----+----+----+----+----+----+
	   |               LF (0x0A)               |
	   +----+----+----+----+----+----+----+----+
*/
package ax594

import (
	"errors"
	"fmt"
	"math"
	"math/bits"
	"strconv"
	"strings"
)

// ErrInvalidData is returned when invalid data is encountered.
var ErrInvalidData = errors.New("invalid data")

// Measurement is a single multimeter measurement.
type Measurement struct {
	Value                     float64 // The value shown on the multimeter
	Prefix, Unit              string  // The prefix and unit shown on the multimeter
	dc, ac, beep, diode       bool    // Flags shown on the multimeter
	auto, min, max, hold, rel bool    // Ranging mode of the multimeter
	bar0                      bool    // First element of the bottom bar on the multimeter
	bar                       int8    // The bottom bar on the multimeter
	decimalPlace              int     // The decimal place shown
}

// NewMeasurement returns a decoded measurement.
func NewMeasurement(data []byte) (*Measurement, error) {
	// Check sample
	if len(data) != 14 {
		return nil, ErrInvalidData
	}

	// Construct measurement
	m := &Measurement{
		auto:  checkBit(data[7], 5),
		dc:    checkBit(data[7], 4),
		ac:    checkBit(data[7], 3),
		rel:   checkBit(data[7], 2),
		hold:  checkBit(data[7], 1),
		bar0:  checkBit(data[7], 0),
		max:   checkBit(data[8], 5),
		min:   checkBit(data[8], 4),
		beep:  checkBit(data[9], 3),
		diode: checkBit(data[9], 2),
		bar:   int8(data[11]),
	}

	// Decode the value
	if string(data[1:5]) == "?0:?" {
		m.Value = math.NaN()
	} else {
		i, err := strconv.Atoi(string(data[0:5]))
		if err != nil {
			return nil, fmt.Errorf("invalid value %q: %w", data[0:5], err)
		}

		m.Value = float64(i)
	}

	// Set the decimal place of the value
	if c := bits.LeadingZeros8(data[6]&0x07) - 4; c != 4 {
		m.decimalPlace = c
		m.Value /= math.Pow10(c)
	}

	m.setPrefix(data)
	m.setUnit(data)

	return m, nil
}

func (m *Measurement) setPrefix(data []byte) {
	switch {
	case checkBit(data[8], 1):
		m.Prefix = "n"
	case checkBit(data[9], 7):
		m.Prefix = "µ"
	case checkBit(data[9], 6):
		m.Prefix = "m"
	case checkBit(data[9], 5):
		m.Prefix = "k"
	case checkBit(data[9], 4):
		m.Prefix = "M"
	}
}

func (m *Measurement) setUnit(data []byte) {
	switch {
	case checkBit(data[9], 1):
		m.Unit = "%"
	case checkBit(data[10], 7):
		m.Unit = "V"
	case checkBit(data[10], 6):
		m.Unit = "A"
	case checkBit(data[10], 5):
		m.Unit = "Ω"
	case checkBit(data[10], 3):
		m.Unit = "Hz"
	case checkBit(data[10], 2):
		m.Unit = "F"
	case checkBit(data[10], 1):
		m.Unit = "°C"
	case checkBit(data[10], 0):
		m.Unit = "°F"
	}
}

// Float returns the value as a floating point value.
// This includes conversion for the SI Prefix, so 6 mV will return 6e-06.
func (m *Measurement) Float() float64 {
	p := 0

	switch m.Prefix {
	case "n":
		p = -9
	case "µ":
		p = -6
	case "m":
		p = -3
	case "k":
		p = 3
	case "M":
		p = 6
	}

	return m.Value * math.Pow10(p)
}

// SI returns the prefix and unit for this measurement.
func (m *Measurement) SI() string {
	return m.Prefix + m.Unit
}

// String returns the measurement as a readable string.
func (m *Measurement) String() string {
	return fmt.Sprintf(
		m.formatString()+" %s (%s)",
		m.Value,
		m.SI(),
		strings.Join(m.Flags(), ", "),
	)
}

// Flags returns a list of human-readable flags.
func (m *Measurement) Flags() (fl []string) {
	if m.auto {
		fl = append(fl, "auto")
	}

	if m.dc {
		fl = append(fl, "DC")
	}

	if m.ac {
		fl = append(fl, "AC")
	}

	if m.rel {
		fl = append(fl, "rel")
	}

	if m.hold {
		fl = append(fl, "hold")
	}

	if m.min {
		fl = append(fl, "min")
	}

	if m.max {
		fl = append(fl, "max")
	}

	if m.beep {
		fl = append(fl, "beep")
	}

	if m.diode {
		fl = append(fl, "diode")
	}

	return fl
}

// formatString returns the format string to format the value like on the display.
func (m *Measurement) formatString() string {
	return fmt.Sprintf("%%%d.%df", 5-m.decimalPlace, m.decimalPlace)
}

// checkBit returns true if a bit in a given byte is set.
func checkBit(d byte, n uint8) bool {
	return (d>>n)&1 == 1
}
