// Copyright 2018 Silke Hofstra
//
// Licensed under the EUPL
//

// ax594_reader is a simple utility that shown measurement values from the AXIOMET AX-594.
package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/tarm/serial"

	"gitlab.snt.utwente.nl/silke/ax594"
)

const (
	retryDelay = 5 * time.Second
	baudRate   = 2400
	csvPerm    = 0o644
)

func reconnect(dev string, config *serial.Config) (r *ax594.Reader) {
	for {
		port, err := serial.OpenPort(config)
		if err == nil {
			r = ax594.NewReader(port)
			break
		}

		log.Printf("Connection to %s lost, reconnecting...", dev)
		time.Sleep(retryDelay)
	}

	return
}

func main() { //nolint:funlen
	var f *os.File

	// Show microseconds in log
	log.SetFlags(log.Flags() | log.Lmicroseconds)

	// Command line options
	dev := flag.String("dev", "/dev/ttyUSB0", "Serial device")
	csv := flag.String("csv", "", "Output to a CSV file")
	flag.Parse()

	// Open serial interface
	port := &serial.Config{Name: *dev, Baud: baudRate}

	s, err := serial.OpenPort(port)
	if err != nil {
		log.Fatal(err)
	}
	defer s.Close()

	if *csv != "" {
		f, err = os.OpenFile(*csv, os.O_APPEND|os.O_CREATE|os.O_WRONLY, csvPerm)
		if err != nil {
			log.Print(err)
			return
		}

		defer f.Close()

		log.Printf("Writing measurements to %q", *csv)
	}

	// Start reading data
	r := ax594.NewReader(s)

	for {
		// Read data from serial interface
		m, err := r.Read()
		if err != nil {
			s.Close() //nolint:gosec

			// Device is gone, attempt to reconnect
			if errors.Is(err, io.EOF) {
				r = reconnect(*dev, port)
				continue
			}

			// Unrecoverable error
			log.Print("Error reading from meter: ", err)

			break
		}

		if *csv == "" {
			log.Print(m)
		} else {
			line := fmt.Sprintf("%s,%f,%s,%s\n",
				time.Now().Format(time.RFC3339Nano), m.Value, m.SI(), strings.Join(m.Flags(), "|"))

			if _, err := f.WriteString(line); err != nil {
				log.Print("Error writing to file: ", err)
			}

			_ = f.Sync()
		}
	}
}
