package main

import (
	_ "embed"
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/websocket"

	"gitlab.snt.utwente.nl/silke/ax594"
)

const (
	clientMapSize = 128
	chanBufSize   = 16
)

//go:embed index.html
var index []byte

//nolint:gomnd,gochecknoglobals
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(_ *http.Request) bool {
		return true
	},
}

// Message represents a websocket message.
type Message struct {
	Value float64  `json:"value"`
	Unit  string   `json:"unit"`
	Flags []string `json:"flags"`
}

// NewMessage creates a message based on an ax594.Measurement.
func NewMessage(m *ax594.Measurement) *Message {
	return &Message{
		Value: m.Float(),
		Unit:  m.Unit,
		Flags: m.Flags(),
	}
}

// Server is a simple server that supports multiplexing a stream of ax594.Measurements
// to multiple clients receiving Messages.
type Server struct {
	mutex         sync.Mutex
	clients       map[int]chan *Message
	clientCounter int
}

// NewServer initializes a new Server.
func NewServer() *Server {
	return &Server{
		clients: make(map[int]chan *Message, clientMapSize),
	}
}

// Multiplex multiplexes a channel of ax594.Measurement values
// and sends them to all clients as Message values.
func (s *Server) Multiplex(ch <-chan *ax594.Measurement) {
	for m := range ch {
		s.publish(NewMessage(m))
	}
}

func (s *Server) publish(m *Message) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	for _, ch := range s.clients {
		ch <- m
	}
}

func (s *Server) newClient() (int, <-chan *Message) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	id := s.clientCounter
	ch := make(chan *Message, chanBufSize)

	s.clientCounter++
	s.clients[id] = ch

	return id, ch
}

func (s *Server) closeClient(id int) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	ch, ok := s.clients[id]
	if !ok {
		return
	}

	delete(s.clients, id)

	if ch != nil {
		close(ch)
	}
}

// IndexHandler handles requests for the index page.
func (s *Server) IndexHandler(w http.ResponseWriter, _ *http.Request) {
	_, _ = w.Write(index)
}

// WebSocketHandler handles requests for the websocket.
func (s *Server) WebSocketHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return
	}

	id, ch := s.newClient()

	log.Printf("New client: %v (%s)", id, r.RemoteAddr)

	for m := range ch {
		err = conn.WriteJSON(m)
		if err != nil {
			log.Printf("Error writing to client %v: %s", id, err)

			s.closeClient(id)

			return
		}
	}
}
