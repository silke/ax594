// Copyright 2022 Silke Hofstra
//
// Licensed under the EUPL
//

// ax594_reader is a simple utility that shown measurement values from the AXIOMET AX-594.
package main

import (
	"context"
	"errors"
	"flag"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/tarm/serial"

	"gitlab.snt.utwente.nl/silke/ax594"
)

const (
	retryDelay = 5 * time.Second
	baudRate   = 2400
)

func read(ch chan<- *ax594.Measurement, dev string, config *serial.Config) {
	s, r := reconnect(dev, config)

	for {
		// Read data from serial interface
		err := r.Run(context.Background(), ch)
		if err == nil {
			break
		}

		// Device is gone, attempt to reconnect
		if errors.Is(err, io.EOF) {
			s.Close() //nolint:gosec
			s, r = reconnect(dev, config)

			continue
		}

		// Unrecoverable error
		log.Fatal("Error reading from meter: ", err)
	}
}

func reconnect(dev string, config *serial.Config) (s *serial.Port, r *ax594.Reader) {
	var err error

	for {
		s, err = serial.OpenPort(config)
		if err == nil {
			r = ax594.NewReader(s)
			break
		}

		log.Printf("Connection to %s lost, reconnecting...", dev)
		time.Sleep(retryDelay)
	}

	return
}

func main() {
	var dev, addr string

	// Command line options
	flag.StringVar(&dev, "dev", "/dev/ttyUSB0", "Serial device")
	flag.StringVar(&addr, "addr", ":8080", "Listen address")
	flag.Parse()

	// Open serial interface
	port := &serial.Config{Name: dev, Baud: baudRate}

	// Measurement channel
	ch := make(chan *ax594.Measurement)

	// Webserver
	s := NewServer()

	http.HandleFunc("/", s.IndexHandler)
	http.HandleFunc("/ws", s.WebSocketHandler)

	go read(ch, dev, port)
	go s.Multiplex(ch)

	log.Printf("Listening on %s", addr)
	log.Fatal(http.ListenAndServe(addr, nil)) //nolint:gosec
}
