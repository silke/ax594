package ax594

import (
	"bufio"
	"context"
	"fmt"
	"io"
)

// Reader is a simple Measurement reader.
type Reader struct {
	r *bufio.Reader
}

// NewReader initializes a Reader based on an io.Reader.
func NewReader(r io.Reader) *Reader {
	return &Reader{r: bufio.NewReader(r)}
}

// Read a single measurement.
func (r *Reader) Read() (*Measurement, error) {
	data, err := r.r.ReadBytes('\n')
	if err != nil {
		return nil, fmt.Errorf("cannot read data: %w", err)
	}

	return NewMeasurement(data)
}

// Run the Reader, writing a stream of measurements to the given channel.
func (r *Reader) Run(ctx context.Context, ch chan<- *Measurement) error {
	for {
		select {
		case <-ctx.Done():
			return nil
		default:
			m, err := r.Read()
			if err != nil {
				return err
			}

			ch <- m
		}
	}
}
