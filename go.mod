module gitlab.snt.utwente.nl/silke/ax594

go 1.22

require (
	github.com/gorilla/websocket v1.5.1
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
)

require (
	golang.org/x/net v0.24.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
)
